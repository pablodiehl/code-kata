package.path = package.path .. ";./?.lua"
local test = require('gambiarra')
package.path = package.path .. ";../?.lua"
local karate = require("02_karate_chop_original")

test('Check Chop Function', function()
  ok(-1 == karate.chop(3, {}), 'Number should not be in the array')
  ok(-1 == karate.chop(3, {1}), 'Number should not be in the array')
  ok(0 == karate.chop(1, {1}), 'Number should be in the array')
    
  ok(0 == karate.chop(1, {1, 3, 5}), 'Number should be in the array')
  ok(1 == karate.chop(3, {1, 3, 5}), 'Number should be in the array')
  ok(2 == karate.chop(5, {1, 3, 5}), 'Number should be in the array')
  ok(-1 == karate.chop(0, {1, 3, 5}), 'Number should not be in the array')
  ok(-1 == karate.chop(2, {1, 3, 5}), 'Number should not be in the array')
  ok(-1 == karate.chop(4, {1, 3, 5}), 'Number should not be in the array')
  ok(-1 == karate.chop(6, {1, 3, 5}), 'Number should not be in the array')
    
  ok(0 == karate.chop(1, {1, 3, 5, 7}), 'Number should be in the array')
  ok(1 == karate.chop(3, {1, 3, 5, 7}), 'Number should be in the array')
  ok(2 == karate.chop(5, {1, 3, 5, 7}), 'Number should be in the array')
  ok(3 == karate.chop(7, {1, 3, 5, 7}), 'Number should be in the array')
  ok(-1 == karate.chop(0, {1, 3, 5, 7}), 'Number should not be in the array')
  ok(-1 == karate.chop(2, {1, 3, 5, 7}), 'Number should not be in the array')
  ok(-1 == karate.chop(4, {1, 3, 5, 7}), 'Number should not be in the array')
  ok(-1 == karate.chop(6, {1, 3, 5, 7}), 'Number should not be in the array')
  ok(-1 == karate.chop(8, {1, 3, 5, 7}), 'Number should not be in the array')
end)