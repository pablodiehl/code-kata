--[[
  
  Kata02: Karate Chop

  After writing my original solution for the task (see the script 02_karate_chop_original), I decided to write another solution, this time using a recursive function.
  
  Note: This solution uses Lua 5.3 resources.
]]
local M = {}

local function chop(value_to_find, array, start_point, end_point)
  -- Set default values if the start and end points are nil
  start_point = start_point or 1
  end_point = end_point or #array
  
  if start_point > end_point then
    return -1
  end
    
  local middle_point = math.floor((start_point + end_point) / 2)
  if array[middle_point] > value_to_find then
    return chop(value_to_find, array, start_point, middle_point - 1)
  elseif array[middle_point] < value_to_find then
    return chop(value_to_find, array, middle_point + 1, end_point)
  end
  
  -- Returning the value minus one here because arrays in Lua starts with index 1, not 0
  return middle_point - 1
end

M.chop = chop
 
return M